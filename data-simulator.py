from firebase import Firebase
import numpy as np
import random
import json

f = Firebase("https://bt3103-test.firebaseio.com/searchRecords")

stops = ["AS7", "BIZ2", "Botanic Gardens MRT", "BTC", "Central Library", "College Green Hostel", "COM2", "Computer Centre", "EA", 
	"Kent Ridge Bus Terminal", "Kent Ridge MRT", "Kent Vale", "LT13", "LT29", "Museum", "Opp HSSML", "Opp Kent Ridge MRT", "Opp NUSS",
	"Opp PGP Hse No 12", "Opp UHall", "Opp UHC", "Opp YIH", "PGP Hse No 12", "PGP Hse No 14 and No 15", "PGP Hse No 7", "PGPR", 
	"Prince George's Park", "Raffles Hall", "S17", "UHall", "UHC", "UTown", "Ventus", "YIH"]

for _ in range(500):
	print(_)
	ss = random.choice(stops)
	es = random.choice(stops)

	while es == ss:
		es = random.choice(stops)
	
	br = ss + " - " + es
	st = random.randint(1509494400, 1510272000) # unix time between 11/01/2017 @ 12:00am (UTC) and 11/10/2017 @ 12:00am (UTC)
	awt = max(int(round(np.random.normal(5, 3))),0)
	ac = max(int(round(np.random.normal(15, 5))),0)
	f.push({"bus_route": br, "start_stop": ss, "end_stop": es, "search_time": st, "ave_waiting_time": awt, "ave_crowdedness": ac})



# r = f.push()


# print "52:54:00:%02x:%02x:%02x" % (
#         random.randint(0, 255),
#         random.randint(0, 255),
#         random.randint(0, 255),
# )









# f = Firebase('https://bt3103-test.firebaseio.com/crowdedness')
# Generate some data and push into firebase database, this part only needs to be run once
# Comment out after first run
# r = f.push({"user_device":"1", "bus_wifi": "A1", "first_seen": "1506562200"})
# r = f.push({"user_device":"2", "bus_wifi": "A1", "first_seen": "1506562260"})
# r = f.push({"user_device":"3", "bus_wifi": "A1", "first_seen": "1506562260"})
# r = f.push({"user_device":"4", "bus_wifi": "A1", "first_seen": "1506562320"})
# r = f.push({"user_device":"5", "bus_wifi": "A1", "first_seen": "1506562320"})
# r = f.push({"user_device":"6", "bus_wifi": "A1", "first_seen": "1506562320"})
# r = f.push({"user_device":"7", "bus_wifi": "A1", "first_seen": "1506562380"})
# r = f.push({"user_device":"8", "bus_wifi": "A1", "first_seen": "1506562380"})
# r = f.push({"user_device":"9", "bus_wifi": "A1", "first_seen": "1506562380"})
# r = f.push({"user_device":"10", "bus_wifi": "A1", "first_seen": "1506562380"})
# r = f.push({"user_device":"11", "bus_wifi": "A1", "first_seen": "1506562440"})
# r = f.push({"user_device":"12", "bus_wifi": "A1", "first_seen": "1506562440"})
# r = f.push({"user_device":"13", "bus_wifi": "A1", "first_seen": "1506562440"})
# r = f.push({"user_device":"14", "bus_wifi": "A1", "first_seen": "1506562440"})
# r = f.push({"user_device":"15", "bus_wifi": "A1", "first_seen": "1506562440"})
# r = f.push({"user_device":"1", "bus_wifi": "A1", "first_seen": "1506562260"})
# r = f.push({"user_device":"1", "bus_wifi": "A1", "first_seen": "1506562320"})

# # get all the subtrees in json
# print(f.get())

# # get the node name
# print(f.name())

# # return a child path
# print(f.child("-KuyqbkROhcov5IUFnjA"))

# # return parent node
# print(f.parent())





# r = f.push({"Mathmatics Induction" : {"price": "1000", "publisher" : "SWR"}})
# print(r)

