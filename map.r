library(rjson)
library(httr)
library(jsonlite)
library(htmltools)
library(shiny)
library(anytime)
library(shinythemes)
library(DT)
library(shinyjs)
library(leaflet)
#load bus stop 
geo_bus_stops = GET('https://liu-siyuan.firebaseio.com/geomap.json')
geo_bus_stops_json = fromJSON(content(geo_bus_stops, 'text'))
geo_bus_stops_table = as.data.frame(geo_bus_stops_json[[1]])

#the function should support the following:
#input: origin name, destination name, mode of transportation (Campus bus,Car,Bicycle,On foot)
carRouteSearch <- function(origin_name,destination_name) {
  origin <- geo_bus_stops_table[gsub(" ","",geo_bus_stops_table$name)==gsub(" ","",origin_name),]
  destination <- geo_bus_stops_table[gsub(" ","",geo_bus_stops_table$name)==gsub(" ","",destination_name),]
  url <- paste0("http://router.project-osrm.org/route/v1/driving/", 
                 origin$lon,",",origin$lat,";",destination$lon,",",destination$lat,"?overview=full")
  system.time({
    route <- rjson::fromJSON(file = url)
  })
  
  decode <- function(str, multiplier=1e5){
    
    if (!require(bitops)) stop("Package: bitops required.")
    if (!require(sp)) stop("Package: sp required.")
    
    truck <- 0
    trucks <- c()
    carriage_q <- 0
    
    for (i in 0:(nchar(str)-1)){
      ch <- substr(str, (i+1), (i+1))
      x <- as.numeric(charToRaw(ch)) - 63
      x5 <- bitShiftR(bitShiftL(x, 32-5), 32-5)
      truck <- bitOr(truck, bitShiftL(x5, carriage_q))
      carriage_q <- carriage_q + 5
      islast <- bitAnd(x, 32) == 0
      if (islast){
        negative <- bitAnd(truck, 1) == 1
        if (negative) truck <- -bitShiftR(-bitFlip(truck), 1)/multiplier
        else truck <- bitShiftR(truck, 1)/multiplier
        trucks <- c(trucks, truck)
        carriage_q <- 0
        truck <- 0
      }
    }
    lat <- trucks[c(T,F)][-1]
    lng <- trucks[c(F,T)][-1]
    res <- data.frame(lat=c(trucks[1],cumsum(lat)+trucks[1]), 
                      lng=c(trucks[2],cumsum(lng)+trucks[2]))
    
    coordinates(res) <- ~lng+lat
    proj4string(res) <- CRS("+init=epsg:4326")
    return(SpatialLines(list(Lines(Line(res), 1)), CRS("+init=epsg:4326")))
  }
  
  path <- decode(route$routes[[1]]$geometry, multiplier=1e5)
  s <- route$routes[[1]]$duration
  kms <- round(route$routes[[1]]$distance/1000, 1)
  
  routelabel <- paste(sep="<br/>",
                      paste0("Distance: ",kms, "kms"),
                      paste0("Estimated travel time: ",s%/%60, "m ", round(s%%60,0), "s"),
                      paste0("<b><a href = ",paste0("http://www.google.com/maps/dir/",origin$lat,",",origin$lon,"/",destination$lat,",",destination$lon),">Open for details on Google Map</a></b>")

  )
  #create a basic map
  m <- leaflet(width="100%") %>% 
    addTiles()  %>% 
    addPolylines(data=path, popup=routelabel,popupOptions = popupOptions(keepInView = TRUE)) %>%
    
    addMarkers(lng=origin$lon, lat=origin$lat, label=htmlEscape(origin$name)) %>%
    addMarkers(lng=destination$lon, lat=destination$lat, label=htmlEscape(destination$name))%>%
    addPopups(
      origin$lon,origin$lat,
      routelabel
    )
  return(m)
}
#demo
ui <- fluidPage(
  leafletOutput("mymap")
)

server <- function(input,output,session) {
  o <- geo_bus_stops_table[1,]
  d <- geo_bus_stops_table[2,]
  output$mymap <- renderLeaflet({carRouteSearch(o$name,d$name)})
}

shinyApp(ui,server)